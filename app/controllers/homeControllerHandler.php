<?php
/**
 * Gestion poney handler (pour appel AJAX)
 *
 * @author Jonathan Rioux		
 * @version 1.0
 */

	require_once '../app/config.php';

	require_once ROOT .'/app/core/tools/ChromePhp.php';
	require_once ROOT ."/app/models/Connexion.php";
	require_once ROOT ."/app/models/FournisseursManager.php";

	if($_POST['action'] == "getListFromCrayon")
	{
		$crayon = $_POST['crayon'];

		$fournisseursM = new FournisseursManager("Inventairecrayons");
		$tousLesFournisseurs = $fournisseursM->getListFromCrayon($crayon);

		echo json_encode($tousLesFournisseurs);
		}
