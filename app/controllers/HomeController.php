<?php
/**
 * Gestion fournisseur handler (pour appel AJAX)
 *
 * @author Jonathan Rioux
 * @version 1.0
 */

include ROOT."./app/models/Connexion.php"; // instructions qui permettront les accès à la BD
include ROOT."./app/core/View.php";
include ROOT."./app/models/CrayonsManager.php";

	class HomeController /*extends controller*/ {

		private $_view;

		protected $_controller = DEFAULT_CONTROLLER; //Par défaut le contrôleur est //home//
    protected $_method = DEFAULT_METHOD;    //Par défaut l'action est //index//
    protected $_params = [];

    function __construct(Array $url = array()) {
			if(isset($url) && count($url) > 1){
          throw new Exception("Page introuvable");
          }
          else {
             $this->index("");
          }
      }

    //L'action par défaut de contrôleur, On appel la vue <home/index>
    public function index($nom = 'home/index') {
      //Préparation des modèles
      try {
				$crayonM = new CrayonsManager(DB_NAME);
				}
			catch (Exception $e) {
    		//ChromePhp::log("Je suis dans l'exception de CrayonsManager ...");
        throw new Exception("Impossible de créer un objet Inventairecrayons");
				}
      $outputData = $CrayonM->getList();

      //Appel à la vue
      $this->_view = new View('home/index');
      $this->_view->assign('lesCrayons', $outputData);
      }
    }
