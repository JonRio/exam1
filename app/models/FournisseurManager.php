<?php
include ROOT."./app/models/Individu.php";

class FournisseurManager extends Connexion {

  public function __construct($db) {
    parent::__construct($db);
  }

  public function add(Fournisseur $perso) {
    $q = parent::prepare('INSERT INTO fournisseur(nom, escompte) VALUES(:nom, :escompte)');
    $q->bindValue(':nom', $perso->nom());
    $q->bindValue(':escompte', $perso->escompte(), PDO::PARAM_INT);
    return(parent::execute());
    }


  public function delete(Crayon $perso) {
    parent::exec('DELETE FROM fournisseur WHERE id_fournisseur = '.$perso->id());
    }

  public function get($id) {
    $id = (int) $id;
    $q = parent::query('SELECT id_fournisseur, nom, escompte FROM individu WHERE id_fournisseur = '.$id);
    $donnees = $q->fetch(PDO::FETCH_ASSOC);
    return new Fournisseur($donnees);
    }

  public function getList() {
    $persos = [];
    $q = parent::query('SELECT id_fournisseur, nom, escompte FROM fournisseur ORDER BY nom');
    while ($donnees = $q->fetch(PDO::FETCH_ASSOC)){
      $persos[] = new Fournisseur($donnees);
      }
    return $persos;
    }

  public function getListFromCrayon($crayonToLookFor) {
    $persos = [];


	if($crayonToLookFor == "0")
		$query = parent::query("SELECT id_fournisseur, nom, escompte
    						FROM fournisseur f
    						ORDER BY nom");
	else
    	$query = parent::query("SELECT id_fournisseur, nom, escompte
    						FROM fournisseur f
    						WHERE '".$crayonToLookFor."' = f.crayon
    						ORDER BY nom");


    while ($donnees = $query->fetch(PDO::FETCH_ASSOC)){
       $fournisseur = new Fournisseur($donnees);
       $persos[ ] = $fournisseur;
      }
    return $persos;
    }

  }
