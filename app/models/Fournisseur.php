﻿<?php
class Fournisseur implements JsonSerializable {

  	private $_id_fournisseur;
  	private $_nom;
  	private $_escompte;
  	public function __construct(array $data = array()) {
        if (!empty($data)) {
            $this->hydrate($data);
            }
    	}

  public function hydrate(array $donnees) {
     foreach ($donnees as $key => $value) {
       // On récupère le nom du setter correspondant à l'attribut.
       $method = 'set'.ucfirst($key);
       // Si le setter correspondant existe.
       if (method_exists($this, $method)){
         // On appelle le setter.
         $this->$method($value);
         }
       }
     }

  	public function id()    { return $this->_id_fournisseur; }
  	public function nom()   { return $this->_nom; }
  	public function escompte()  { return $this->_escompte; }
  	public function setId_fournisseur($id) {
    	$this->_id_fournisseur = (int) $id;
    	}

  	public function setNom($nom) {
    	if (is_string($nom) && strlen($nom) <= 1024) {
      		$this->_nom = $nom;
      		}
    	}

	public function setEscompte($escompte) {
    	if (is_string($escompte) && strlen($escompte) <= 1024) {
      		$this->_escompte = $escompte;
      		}
    	}

	public function jsonSerialize () {
        return array(
            'nom'=>$this->_nom,
            'escompte'=>$this->_escompte);
    	}
  }
