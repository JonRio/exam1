<?php
include ROOT."./app/models/Crayon.php";
class CrayonsManager extends Connexion {

  public function __construct($db) {
     parent::__construct($db);
	}

  public function add(Crayon $perso) {
    $q = parent::prepare('INSERT INTO crayon(nom, couleur, longeur, idFournisseur) VALUES(:nom, :couleur, :longeur, :idFournisseur)');
    $q->bindValue(':nom', $perso->nom());
    $q->bindValue(':couleur', $perso->couleur());
    $q->bindValue(':longeur', $perso->longeur(), PDO::PARAM_INT);
    $q->bindValue(':idFournisseur', $perso->idFournisseur());
    $lastId = parent::execute($q);
    return ($lastId);
    }

  public function deleteAllCrayons() {
      parent::exec('DELETE FROM crayon ');
      }

  public function delete(Crayon $perso) {
    parent::exec('DELETE FROM crayon WHERE id_crayon = '.$perso->id());
    }

  public function get($id) {
    $id = (int) $id;
    $q = parent::query('SELECT id_crayon, nom, couleur, longeur, id_fournisseur FROM crayon WHERE id_crayon = '.$id);
    $donnees = $q->fetch(PDO::FETCH_ASSOC);

    return new Crayon($donnees);
    }

  public function getList() {
    $persos = [];
    $query = parent::query('SELECT id_crayon, nom, couleur, longeur, id_fournisseur FROM crayon');

    while ($donnees = $query->fetch(PDO::FETCH_ASSOC)){

       $race = new Crayon($donnees);
       $persos[$crayon->id() ] = $crayon;
      }
    return $persos;
    }

  public function updateLongeur(Crayon $perso) {
    $q = parent::prepare('UPDATE crayon SET longeur = :longeur WHERE id_longeur = :id');
    $q->bindValue(':longeur', $perso->longeur(), PDO::PARAM_INT);
    $q->bindValue(':id', $perso->id(), PDO::PARAM_INT);
    parent::execute($q);
  	}
  }
