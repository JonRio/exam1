<?php
class Crayon implements JsonSerializable  {
    private $_id_crayon;
    private $_nom = "emptyName";
    private $_couleur = "emptyCouleur";
    private $_longeur = 0;
    private $_id_fournisseur;

    public function __construct(array $data = array()) {
      if (!empty($data)) {
            $this->hydrate($data);
            }
    	}

    // Un tableau de données doit être passé à la fonction (d'où le préfixe « array »).
    public function hydrate(array $donnees) {
      foreach ($donnees as $key => $value) {
         // On récupère le nom du setter correspondant à l'attribut.
         $method = 'set'.ucfirst($key);
         // Si le setter correspondant existe.
         if (method_exists($this, $method)){
            // On appelle le setter.
            $this->$method($value);
           }
         }
      }

    public function idCrayon()    { return $this->_id_crayon; }
    public function nom()   { return $this->_nom; }
    public function couleur()  { return $this->_couleur; }
    public function longeur()  { return $this->_longeur; }
    public function idFournisseur()    { return $this->_id_fournisseur; }

    public function setId_crayon($id) {
      $this->_id_crayon = (int) $id;
      }
    public function setNom($nom) {
      if (is_string($nom) && strlen($nom) <= 30) {
        $this->_nom = $nom;
        }
      }
    public function setCouleur($couleur) {
      if (is_string($couleur) && strlen($couleur) <= 1024) {
        $this->_couleur = $couleur;
        }
      }

      // fix la longeur d'un crayons pour qui ne depasse pas 32 centimetres et ne soit pas plus petit que 2 centimetres
    public function setlongeur($longeur) {
   	  $longeur1 = (float) $longeur;
      if ($longeur1 > 32 && $longeur1 <= 2) {
        $this->_longeur = $longeur1;
      	}
      }

      public function setId_fournisseur($id) {
        $this->_id_fournisseur = (int) $id;
        }
    /**
       * Permet de récupérer des éléments en Javascript (sérialisation)
         * @return array Retourne la sérialisation de l'individu
       */
   public function jsonSerialize () {
        return array(
          'nom'=>$this->_nom,
          'couleur'=>$this->_description,
          'longeur'=>$this->_taille,
          'idFournisseur'=>$this->_id_fournisseur);
        }
     }
