<?php
/**
 * Fichier de configuratiom
 *
 * @author Jonathan Rioux
 * @version 1.0
 */

 	//Définir le répertoire de votre site
  define('BASEDIR', '/exam1/');
  define('ROOT', $_SERVER['DOCUMENT_ROOT']  . BASEDIR);
  define('DEFAULT_CONTROLLER', 'Home');
  define('DEFAULT_METHOD', 'index');
  define('ERROR_CONTROLLER', 'Error');

 	//Les détails de la base de données si utilisées
	define('DB_TYPE', 'mysql');
	define('DB_HOST', 'localhost');
	define('DB_NAME', 'Inventairecrayons');
  define('DB_PORT', 8889);
	define('DB_USER', 'root');
	define('DB_PASS', 'root');
