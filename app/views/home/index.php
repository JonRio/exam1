<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

 <?php include(ROOT."./app/views/header.php");?>

<div id="fournisseurs">
   <h1>Liste des fournisseurs</h1>
     <h2>Liste des fournisseurs de crayons disponibles: </h2>

      <select id="listeFournisseur" name="fournisseurChoisi" >
    <?php
      echo "<option value='0'>Tous</option>";
      foreach ($lesFournisseurs as $key) {
          echo sprintf("<option value=%s>%s</option> ", $key->id(), utf8_encode($key->nom()));
        unset($key);
        }
      ?>
    </select>
    <br />
    <br />
    <button>Get Fournisseur</button>

    <br />
  </div>

  <div id="fournisseurListe">Liste des founisseurs</div>

  <script>

      $(document).ready(function() {
      $("button").click(function(){

        var selectionnedFournisseurId = $('#listeFournisseur option:selected').attr('value');

        if (selectionnedFournisseurId == '') {
            alert("Aucun fournisseur sélectionnée");
          } else {
              $.post(
                './public/routageAjax.php',
                {
                    controller : "homeControllerHandler",
                    action : "getListFromCrayon",
                    fournisseur : selectionnedFournisseurId // Nous supposons que ce formulaire existe dans le DOM.
                },
                nom_fonction_retour, // Nous renseignons uniquement le nom de la fonction de retour.
                'json' // Format des données reçues.
              );

            function nom_fonction_retour(msg) {

              //Remarque importante
              //Nous pouvons récupérer les attributs privés nom et robe grace a la méthode
              //  public function jsonSerialize () que nous avons ajouté a notre
              //  classe Individu implements JsonSerializable

              var txt = "<b>";
              txt = txt + " Nom    &emsp;     Longueur <br/><br/>";
              for (var property1 in msg) {
                    txt = txt +
                      msg[property1].nom + "   &emsp;    " +
                      msg[property1].longeur +"<br/>";
                  }
              txt = txt + "</b>";
                $("#fournisseurListe").html(txt);

              }

          } //if else
        }); //$("#submit").click(function() {
      });  //$(document).ready(function() {
    </script>

<?php include(ROOT."./app/views/footer.php");?>
