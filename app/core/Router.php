<?php
class Router {
  private $_ctrl;
  private $view;

  public function routeReq() {
    try {
          $url = Array('');

          if(isset($_GET['url']) && ($_GET['url']!="")){

            $url = explode('/', filter_var($_GET['url'], FILTER_SANITIZE_URL));

            $controller = ucfirst(strtolower($url[0]));
            $controllerClass = $controller."Controller";
            $controllerFile = ROOT."./app/controllers/".$controllerClass.".php";

            if(file_exists($controllerFile)) {
                require_once($controllerFile);
                $this->_ctrl =  new $controllerClass($url);
              }
              else
                throw new Exception("Page introuvable");
            }
            else {
                $defaultControllerClass = DEFAULT_CONTROLLER."Controller";
                $defaultControllerFile = ROOT."./app/controllers/".$defaultControllerClass.".php";
                require_once($defaultControllerFile);
                $this->_ctrl =  new $defaultControllerClass($url);
                }
      }
      catch (Exception $e){


          $errorControllerClass = ERROR_CONTROLLER."Controller";
          $errorControllerFile = ROOT."./app/controllers/".$errorControllerClass.".php";

          require_once($errorControllerFile);
          $this->_ctrl =  new $errorControllerClass($url);
          }
    }
  }
