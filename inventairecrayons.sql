-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 17 oct. 2018 à 15:43
-- Version du serveur :  10.1.34-MariaDB
-- Version de PHP :  7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `inventairecrayons`
--

-- --------------------------------------------------------

--
-- Structure de la table `crayon`
--

CREATE TABLE `crayon` (
  `id_crayon` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `couleur` varchar(64) NOT NULL,
  `longeur` int(11) NOT NULL,
  `id_fournisseur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `crayon`
--

INSERT INTO `crayon` (`id_crayon`, `nom`, `couleur`, `longeur`, `id_fournisseur`) VALUES
(1, 'Pentaire', 'Rouge', 14, 1),
(2, 'Sorel', 'Vert', 11, 1),
(3, 'Patricia', 'Jaune', 16, 2),
(4, 'Mongo', 'Bleu', 11, 1),
(5, 'Patrol', 'Bleu', 10, 2),
(6, 'Zinki', 'Rouge', 3, 2),
(7, 'Sorel', 'Vert', 11, 1);

-- --------------------------------------------------------

--
-- Structure de la table `fournisseur`
--

CREATE TABLE `fournisseur` (
  `id_fournisseur` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `escompte` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fournisseur`
--

INSERT INTO `fournisseur` (`id_fournisseur`, `nom`, `escompte`) VALUES
(1, 'La maison de Jade', 35),
(2, 'Polux inc', 23);

-- --------------------------------------------------------

--
-- Structure de la table `privileges`
--

CREATE TABLE `privileges` (
  `id_privilege` int(11) NOT NULL,
  `description` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `privileges`
--

INSERT INTO `privileges` (`id_privilege`, `description`) VALUES
(1, 'administrateur'),
(2, 'clientWeb'),
(3, 'usager');

-- --------------------------------------------------------

--
-- Structure de la table `usagers`
--

CREATE TABLE `usagers` (
  `id_usager` int(11) NOT NULL,
  `nom` varchar(64) NOT NULL,
  `prenom` varchar(64) NOT NULL,
  `ident` varchar(32) NOT NULL,
  `id_privileges` int(11) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `usagers`
--

INSERT INTO `usagers` (`id_usager`, `nom`, `prenom`, `ident`, `id_privileges`, `password`) VALUES
(1, 'Dube', 'Alain', 'aladub', 1, ''),
(2, 'Web', 'Web', 'webweb', 2, ''),
(3, 'Pierre', 'Martin', 'marPie', 3, ''),
(4, 'Dubuis', 'Caryne', 'cardub', 3, '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `crayon`
--
ALTER TABLE `crayon`
  ADD PRIMARY KEY (`id_crayon`);

--
-- Index pour la table `fournisseur`
--
ALTER TABLE `fournisseur`
  ADD PRIMARY KEY (`id_fournisseur`);

--
-- Index pour la table `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id_privilege`);

--
-- Index pour la table `usagers`
--
ALTER TABLE `usagers`
  ADD PRIMARY KEY (`id_usager`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
